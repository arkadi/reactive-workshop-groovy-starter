@Grapes([
    @Grab('io.reactivex:rxgroovy:1.0.0'),
    @Grab('org.codehaus.gpars:gpars:1.2.1'),
    @Grab('com.github.groovy-wslite:groovy-wslite:1.1.0')])

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import rx.Observable
import rx.Subscriber
import rx.schedulers.Schedulers
//import java.util.concurrent.ForkJoinPool
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import jsr166y.ForkJoinPool
import static groovyx.gpars.GParsPool.executeAsync
import static groovyx.gpars.GParsPool.withExistingPool
import static groovyx.gpars.GParsPool.withPool
import wslite.rest.RESTClient

String participant = 'change-me'
String clientId = '' // obtain your own at https://instagram.com/developer
String api = 'https://api.instagram.com/v1'
String ui = 'http://rxdisplay.neueda.lv/in'
int interval = 5 // seconds
int count = 10 // count of tagged media to return
def tags = 'invent your own'.split(' ')
int limit = 3 // number of tags per observable stream

// https://github.com/jwagenleitner/groovy-wslite#rest
def instaRest = new RESTClient(api)
def uiRest = new RESTClient(ui)
def fjp = new ForkJoinPool(limit)
def scheduler = Schedulers.newThread()

@EqualsAndHashCode @ToString(includeNames=true)
class Location { Double latitude; Double longitude; Integer id; String name; }
@EqualsAndHashCode(includes = 'url') @ToString
class Media { String tag; String url; Location location; String participant; }

def unjson(json) {
    // convert Instagram JSON into Media[]
}

def media = { String tag ->
    def json = instaRest.*****
    def media = unjson(json)
    media.each { it.tag = tag }
    media
}

println 'Starting...'

// step one: single-threaded HTTP fetch
def ticker = Observable.*****
def obstags = Observable.*****(tags.take(limit))
def nested /*Observable<Observable<List<Media>>>*/ = ticker.map { _seq ->
    /*Observable<List<Media>>*/ obstags.***** { tag ->
        Observable.*****(media(tag)) // and maybe error recovery
    }
}
def list /*Observable<List<Media>>*/ = Observable.*****(nested)

list.subscribe /* print */

// step two: flatten
def instagram /*Observable<Media>*/ = Observable.create { Subscriber<Media> observer ->
    list.subscribe(3-args)
}

// step three: send to UI
instagram.subscribe { image ->
    image.participant = participant
    uiRest.*****(new JsonBuilder(image).toString())
}

// step four: deduplicate and verify
long start = System.currentTimeMillis()
def limited = instagram.takeWhile *****
limited.count().forEach *****
*****


Thread.sleep(100000)
