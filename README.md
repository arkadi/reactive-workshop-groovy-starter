### Reactive workshop Groovy starter

Follow [the steps](https://gist.github.com/arkadijs/a2a6901d34272fbbdaa7) and run the starter:

    groovy instagram.groovy

First run will be slow - the dependencies must be downloaded.
